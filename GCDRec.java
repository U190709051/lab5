public class GCDRec {
    public static void main(String[] args) {
        int n1= Integer.parseInt(args[0]);
        int n2 = Integer.parseInt(args[1]);

        System.out.println(gcd(n1,n2));
    }

    public static int gcd(int n1, int n2) {

        int min = n1 > n2 ? n2 : n1;

        for (int i = min; i > 1; i--) {
            if (n1 % i == 0 & n2 % i == 0) {
                System.out.print("Greatest common divisor is:");
                return i;
            }
        }
        return 1;
    }
}
