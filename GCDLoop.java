public class GCDLoop {
    public static void main(String[] args) {
        int ebob=1;
        int n1 = Integer.parseInt(args[0]);
        int n2 = Integer.parseInt(args[1]);

        int min = (n1<n2) ? n1 : n2 ;
        for (int i = min ;i>0;i--){
            if (n1 % i == 0 && n2 % i == 0){
                ebob = i;
                break;
            }
        }
        System.out.println("Greatest Common Divisor is " + ebob);
    }
}
